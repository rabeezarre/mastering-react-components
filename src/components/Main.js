import React from "react";
import data from "../utils/data.json";
import Photo from "./Photo";

export default function Main() {
  return (
    <div className="main-container">
      <div className="gallery">
        {data.slice(0, 10).map((photo) => (
          <Photo
            key={photo.id}
            id={photo.id}
            title={photo.title}
            url={photo.url}
          />
        ))}
      </div>
    </div>
  );
}
