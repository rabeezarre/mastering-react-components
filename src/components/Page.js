import React from 'react'
import Header from './Header'
import Main from './Main'
import Footer from './Footer'

export default function Page() {
  return (
    <div>
        <Header title="Colors gallery">
            <div>This is description</div>
        </Header>
        <Main/>
        <Footer title="Created by @rabeezarre"/>
    </div>
  )
}
