import React from "react";

Footer.defaultProps = {
  title: "Footer title",
};

export default function Footer(props) {
  return (
    <div className="footer">
      {props.title}
    </div>
  );
}
