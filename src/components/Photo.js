import React from 'react'
import PropTypes from 'prop-types';

Photo.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default function Photo({id, title, url}) {
  return (
    <div className="card">
      <img src={url} alt={id} className='card_image'/>
      <p className='card_title'>{title}</p>
    </div>
  )
}
