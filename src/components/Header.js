import React from 'react'

Header.defaultProps = {
    title: "Header title",
  };

export default function Header(props) {
  return (
    <>
    <h1>{props.title}</h1>
    {props.children}
    </>
  )
}
